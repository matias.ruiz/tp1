import threading
from main import create_app
import os
from main.utilities.SensorSockets import call_sensors


app = create_app()
app.app_context().push()
from main import db


if __name__ == '__main__':
    db.create_all()
    threading.Thread(target=call_sensors, args=(app, db)).start()
    app.run(debug=True, port=os.getenv("PORT"))
