from .. import db
import datetime as dt
import time
from main.models.Sensor import Sensor

class Seism(db.Model):

    seismId = db.Column(db.Integer, primary_key=True)
    datetime = db.Column(db.DateTime, nullable=False)
    depth = db.Column(db.Integer, nullable=False)
    magnitude = db.Column(db.Numeric, nullable=False)
    latitude = db.Column(db.String(30), nullable=False)
    longitude = db.Column(db.String(30), nullable=False)
    verified = db.Column(db.Boolean, nullable=False)
    sensorId = db.Column(db.Integer, db.ForeignKey('sensor.sensorId'), nullable=False)
    sensor = db.relationship("Sensor", back_populates="seisms", uselist=False, single_parent=True)
# single_parent: Prevenir que el objeto este asociado a mas de un objeto de la otra tabla (Sensor)
# back_populates: Como se llama la relacion desde la clase Sensor


    def __repr__(self):
        return '<Seism:  %r %r %r %r %r>' % (self.datetime, self.magnitude, self.latitude, self.longitude, self.sensorId)


    def to_json(self):
        self.sensor = db.session.query(Sensor).get_or_404(self.sensorId)
        seism_json = {
            'seismId': self.seismId,
            'datetime': str(self.datetime),
            'depth': int(self.depth),
            'magnitude': float(self.magnitude),
            'latitude': str(self.latitude),
            'longitude': str(self.longitude),
            'verified': bool(self.verified),
            'sensorId': self.sensor.to_json()
        }
        return seism_json

    @staticmethod
    def from_json(seism_json):
        seismId = seism_json.get('seismId')
        datetime = dt.datetime.strptime(seism_json.get('datetime'), "%Y-%m-%d %H:%M:%S")
        depth = seism_json.get('depth')
        magnitude = seism_json.get('magnitude')
        latitude = seism_json.get('latitude')
        longitude = seism_json.get('longitude')
        verified = seism_json.get('verified')
        sensorId = seism_json.get('sensorId')

        return Seism(seismId=seismId,
                    datetime=datetime,
                    depth=depth,
                    magnitude=magnitude,
                    latitude=latitude,
                    longitude=longitude,
                    verified=verified,
                    sensorId=sensorId)
