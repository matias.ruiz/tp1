from .. import db
from main.models.User import User


class Sensor(db.Model):

    sensorId = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    ip = db.Column(db.String(30), nullable=False)
    port = db.Column(db.Integer, nullable=False)
    status = db.Column(db.Boolean, nullable=False)
    active = db.Column(db.Boolean, nullable=False)
    userId = db.Column(db.Integer, db.ForeignKey("user.userId"))
    seisms = db.relationship("Seism", back_populates="sensor", single_parent=True)
    user = db.relationship("User", back_populates="sensors", uselist=False, single_parent=True)



    def __repr__(self):
        return '<Sensor: %r %r %r %r %r>' % (self.name, self.ip, self.port, self.status, self.active)

    def to_json(self):
        self.user = db.session.query(User).get_or_404(self.userId)
        sensor_json = {
            'sensorId': self.sensorId,
            'name': str(self.name),
            'ip': str(self.ip),
            'port': int(self.port),
            'status': bool(self.status),
            'active': bool(self.active),
            'userId': self.user.to_json()
        }
        return sensor_json

    @staticmethod
    def from_json(sensor_json):
        sensorId = sensor_json.get('sensorId')
        name = sensor_json.get('name')
        ip = sensor_json.get('ip')
        port = sensor_json.get('port')
        status = sensor_json.get('status')
        active = sensor_json.get('active')
        userId = sensor_json.get('userId')

        return Sensor(sensorId=sensorId,
                      name=name,
                      ip=ip,
                      port=port,
                      status=status,
                      active=active,
                      userId=userId)

