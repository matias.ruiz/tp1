from flask_restful import Resource
from flask import request, jsonify
from .. import db
from main.models import SeismModel
from main.models import SensorModel
import datetime as dt
from flask_jwt_extended import jwt_required, jwt_optional


class VerifiedSeism(Resource):
    @jwt_optional
    def get(self, id):
        seism = db.session.query(SeismModel).get_or_404(id)
        if seism.verified:
            return seism.to_json()
        else:
            return "ACCESS DENIED", 403



class VerifiedSeisms(Resource):
    @jwt_optional
    def get(self):
        try:
            filter = request.get_json().items()
            seisms = db.session.query(SeismModel).filter(SeismModel.verified==True)
            page = 1
            per_page = 7
            for key, value in filter:
                if key == "seismId":
                    seisms = seisms.filter(SeismModel.seismId == value)
                if key == "from_date":
                    value = dt.datetime.strptime(value, "%Y-%m-%d %H:%M:%S")
                    seisms = seisms.filter(SeismModel.datetime >= value)
                if key == "to_date":
                    value = dt.datetime.strptime(value, "%Y-%m-%d %H:%M:%S")
                    seisms = seisms.filter(SeismModel.datetime <= value)
                if key == "magnitude_min":
                    seisms = seisms.filter(SeismModel.magnitude >= value)
                if key == "magnitude_max":
                    seisms = seisms.filter(SeismModel.magnitude <= value)
                if key == "depth_min":
                    seisms = seisms.filter(SeismModel.depth >= value)
                if key == "depth_max":
                    seisms = seisms.filter(SeismModel.depth <= value)
                if key == "sensor_name":
                    seisms = seisms.join(SeismModel.sensor).filter(SensorModel.name.like("%" + str(value) + "%"))

                if key == "page":
                    page = int(value)
                if key == "order_by":
                    if value == "datetime[desc]":
                        seisms = seisms.order_by(SeismModel.datetime.desc())
                    if value == "datetime[asc]":
                        seisms = seisms.order_by(SeismModel.datetime.asc())
                    if value == "sensor_name[desc]":
                        seisms = seisms.join(SeismModel.sensor).order_by(SensorModel.name.desc())
                    if value == "sensor_name[asc]":
                        seisms = seisms.join(SeismModel.sensor).order_by(SensorModel.name.asc())
                    if value == "magnitude[desc]":
                        seisms = seisms.order_by(SeismModel.magnitude.desc())
                    if value == "magnitude[asc]":
                        seisms = seisms.order_by(SeismModel.magnitude.asc())
                    if value == "depth[desc]":
                        seisms = seisms.order_by(SeismModel.depth.desc())
                    if value == "depth[asc]":
                        seisms = seisms.order_by(SeismModel.depth.asc())
            seisms = seisms.paginate(page, per_page, True, 15)
            return jsonify({'verified-seisms': [seism.to_json() for seism in seisms.items],
                            "total_items": seisms.total,
                            "total_pages": seisms.pages,
                            "page": page})
        except Exception as error:
            return str(error), 400



class UnverifiedSeism(Resource):
    @jwt_required
    def get(self, id):
        seism = db.session.query(SeismModel).get_or_404(id)
        if not seism.verified:
            return seism.to_json()
        else:
            return "ACCESS DENIED", 403

    @jwt_required
    def delete(self, id):
        seism = db.session.query(SeismModel).get_or_404(id)
        if not seism.verified:
            db.session.delete(seism)
            db.session.commit()
            return "DELETE COMPLETE", 204
        else:
            return "ACCESS DENIED", 403

    @jwt_required
    def put(self, id):
        seism = db.session.query(SeismModel).get_or_404(id)
        if not seism.verified:
            for key, value in request.get_json().items():
                if key == 'datetime':
                    setattr(seism, key, dt.datetime.strptime(value, "%Y-%m-%d %H:%M:%S"))
                else:
                    setattr(seism, key, value)
            db.session.add(seism)
            db.session.commit()
            return request.get_json(), 201
        else:
            return "ACCESS DENIED", 403


class UnverifiedSeisms(Resource):
    @jwt_required
    def get(self):
        try:
            filter = request.get_json().items()
            seisms = db.session.query(SeismModel).filter(SeismModel.verified==False)
            page = 1
            per_page = 10
            for key, value in filter:
                if key == "seismId":
                    seisms = seisms.filter(SeismModel.seismId == value)

                if key == "from_date":
                    value = dt.datetime.strptime(value, "%Y-%m-%d %H:%M:%S")
                    seisms = seisms.filter(SeismModel.datetime >= value)
                if key == "to_date":
                    value = dt.datetime.strptime(value, "%Y-%m-%d %H:%M:%S")
                    seisms = seisms.filter(SeismModel.datetime <= value)

                if key == "depth":
                    try:
                        seisms = seisms.filter(SeismModel.depth >= value[0], SeismModel.depth <= value[1])
                    except:
                        seisms = seisms.filter(SeismModel.depth == value)
                if key == "magnitude":
                    try:
                        seisms = seisms.filter(SeismModel.magnitude >= value[0], SeismModel.magnitude <= value[1])
                    except:
                        seisms = seisms.filter(SeismModel.magnitude == value)
                if key == "sensorId":
                    seisms = seisms.filter(SeismModel.sensorId == value)
                if key == "page":
                    page = int(value)
                if key == "per_page":
                    per_page = int(value)
                if key == "order_by":
                    if value == "sensor_name[desc]":
                        seisms = seisms.join(SeismModel.sensor).order_by(SensorModel.name.desc())
                    if value == "sensor_name[asc]":
                        seisms = seisms.join(SeismModel.sensor).order_by(SensorModel.name.asc())
                    if value == "datetime[desc]":
                        seisms = seisms.order_by(SeismModel.datetime.desc())
                    if value == "datetime[asc]":
                        seisms = seisms.order_by(SeismModel.datetime.asc())
                    if value == "magnitude[desc]":
                        seisms = seisms.order_by(SeismModel.magnitude.desc())
                    if value == "magnitude[asc]":
                        seisms = seisms.order_by(SeismModel.magnitude.asc())

            seisms = seisms.paginate(page, per_page, True, 10)

            return jsonify({'unverified-seisms': [seism.to_json() for seism in seisms.items],
                            "total_items": seisms.total,
                            "total_pages": seisms.pages,
                            "page": page
                            })
        except Exception as error:
            return str(error), 400
