import socket
import time
import json
import random
from ..models import SeismModel, SensorModel


def create_socket():
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.settimeout(2)
        return s
    except socket.error:
        print("Failed to create socket")
        return None


def call_sensors(app, db):
    with app.app_context():
        socket = create_socket()
        while socket:
            sensors = (db.session.query(SensorModel).filter(SensorModel.active==True).filter(SensorModel.status == True).all())
            for sensor in sensors:
                #print(sensor.port, sensor.ip)
                socket.sendto(b" ", (sensor.ip, sensor.port))
                try:
                    d = socket.recvfrom(1024)[0]
                    seism = SeismModel.from_json_seism(json.loads(d))
                    seism.sensorId = sensor.id
                    seism.verified = random.choice([True, False])
                    db.session.add(seism)
                    db.session.commit()
                except Exception as e:
                    print("Sensor " + sensor.name + " is not responding.")
            time.sleep(3)
