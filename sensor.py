import time, json
from random import uniform, random, randint, uniform
import socket, sys, time


def send_data():
    value_sensor = {
        'datetime': time.strftime(r"%Y-%m-%d %H:%M:%S", time.localtime()),
        'depth': randint(5, 250),
        'magnitude': round(uniform(2.0, 5.5), 1),
        'latitude': uniform(-180, 180),
        'longitude': uniform(-90, 90)
    }
    return value_sensor

serversocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

host = "0.0.0.0"

port = int(sys.argv[1])

serversocket.bind((host, port))

while True:
    print("Listening...")
    data, addr = serversocket.recvfrom(1024)
    print(addr)
    address = addr[0]
    port = addr[1]
    print("Address: %s - Port %d" % (address, port))
    msg = json.dumps(send_data())
    serversocket.sendto(msg.encode(), addr)


