import { Sensor } from "./Sensor.model";
import { Moment } from 'moment';

export interface Iseism{
    seismId: number;
    datetime: Moment;
    depth: number;
    magnitude: number; 
    latitude: string;
    longitude: string; 
    verified: boolean;
    sensorId: number;
    sensor: Sensor;

}

export class Seism implements Iseism{
    constructor(
        public.seismId: number,
        public.datetime: Moment,
        public.depth: number,
        public.magnitude: number,
        public.latitude: string,
        public.longitude: string,
        public.verified: boolean,
        public.sensorId: number,
        public.sensor: Sensor){}
}