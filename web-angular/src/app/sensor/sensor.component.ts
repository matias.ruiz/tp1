import { Component, OnInit } from '@angular/core';
import {Sensor} from './sensor.model'

@Component({
  selector: 'app-sensor',
  templateUrl: './sensor.component.html',
  styleUrls: ['./sensor.component.scss']
})
export class SensorComponent implements OnInit {
  sensors :Array<Sensor>;
  selectedSensor: Sensor;
  constructor() { }

  cargadatos(){
    this.sensors = [
      {
        sensorId: 1,
        name: 'Sensor1',
        status: true,
        active: true,
        
      },
      {
        sensorId: 2,
        name: 'Sensor2',
        status: true,
        active: true, 
      },
      {
        sensorId: 3,
        name: 'Sensor3',
        status: false,
        active: false, 
      },
      {
        sensorId: 3,
        name: 'Sensor3',
        status: false,
        active: true, 
      },
      {
        sensorId: 3,
        name: 'Sensor3',
        status: true,
        active: false, 
      },
     
    ]
  }

  ngOnInit(): void {
    console.log("Carga del componente SENSOR");
    this.cargadatos();
  }
  onSelect(sensor:Sensor): void{
    this.selectedSensor=sensor;
  }

}
