from flask import request
from flask_login import UserMixin
from jwt import decode, exceptions

from main import login_manager


class LoggedUser(UserMixin):
    def __init__(self, id, admin):
        self.id = id
        self.admin = admin

@login_manager.request_loader
def load_user(request):
    if 'access_token' in request.cookies:
        try:
            decoded_token = decode(request.cookies['access_token'], verify=False)
            user_data = decoded_token['identity']
            user = LoggedUser(
                id=user_data['userId'],
                admin=user_data['admin']
            )
            return user
        except exceptions.DecodeError as e:
            pass
        except exceptions.InvalidTokenError as e:
            pass
    return None
