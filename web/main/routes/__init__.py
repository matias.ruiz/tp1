from main.routes.login import login
from main.routes.home import home
from main.routes.users import users
from main.routes.sensors import sensors
from main.routes.seisms_no_verified import seisms_no_verified
from main.routes.seisms_verified import seisms_verified
