from flask import Blueprint, render_template
from flask_login import login_required

home = Blueprint('home', __name__, url_prefix='/')


@home.route('/')
@home.route('/home')
@login_required
def index():
    return render_template('home.html')

